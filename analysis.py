#!/usr/bin/env python3
import argparse
import csv
import numpy as np
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--ppn', type=int, help='participant number')
parser.add_argument('-d', '--distance', type=int, help='searchlight image distance for view')
parser.add_argument('-g', '--groupwise', action='store_true', help='one line per image')
args = parser.parse_args()

# read constants from experiment file
experimentFile = csv.reader(open("data/experiment_{:04d}.csv".format(args.ppn), "r"),
	quoting=csv.QUOTE_NONNUMERIC)
	#"window size",800,600
	#"imageHeight",60
	#"imageDistance",120
for i, row in enumerate(experimentFile):
	if row[0]=="window size":
		width = round(row[1])
		height = round(row[2])
	elif row[0]=="imageDistance":
		imageDistance = round(row[1])
	elif row[0]=="filesIndicesExploration":
		filesIndicesExploration = []
		for i in range(1, 5):
			s = row[i][1:-1]
			filesIndicesExploration.append([int(x) for x in s.split()])
print("window size: {:d}, {:d}, image distance: {:d}".format(width, height, imageDistance))
# get squared distance that is considered upper limit of viewing
d2 = (imageDistance/2)**2
if args.distance:
	d2 = args.distance**2

# read explorations path from exploration file
#explorationFile.writerow(("iMove","time","x","y"))
explorationFile = csv.reader(open("data/exploration_{:04d}.csv".format(args.ppn), "r"),
	quoting=csv.QUOTE_NONNUMERIC)
rows = []
for row in explorationFile:
	rows.append(row)
print("lines read: {:d}".format(len(rows)))
rows.append([0,0,10000,10000]) # move away from all images to ensure last image registers too
	
# image positions
pos = []
for i in range(25):
	pos.append((imageDistance*(i%5-2), imageDistance*(2-i//5)))

# initialize arrays
hits = np.zeros([25], dtype=np.int) # number of consecutive frames in which this image was seen
views = np.zeros([25], dtype=np.int) # number of times this image was seen
#blocks = np.zeros([25], dtype=np.int) # starttimes of views
#times = np.zeros([25], dtype=np.float) # starttimes of views

for iRow, row in enumerate(rows):
	newHits = np.zeros([25], dtype=np.bool) # new images viewed, not viewed in last frame
	if iRow == 0: continue
	try: # interpreting values from file as floats/ints goes in this try block
		iMove, t, *p = row
		iMove = round(iMove)
		x, y = round(p[0]), round(p[1])
		iBlock = iMove//3
		odd = args.ppn%2
		start = (iMove%3)*25 # 0, 25, 50
		
		for i in range(25):
			if (pos[i][0]-x)**2+(pos[i][1]-y)**2 < d2:
				#print("{:d}, move: {:d}: image {:d}, {:d}".format(iRow, iMove, i%5-2, i//5-2))
				newHits[i] = True
		end = np.logical_and(hits, np.logical_not(newHits)) # non hit that was a hit in the prevcious frame
		#print("   hits: " , hits.astype(np.int))
		#print("newhist: " , newHits.astype(np.int))
		#print("    end: " , np.where(end)[0])
		for i in np.where(end)[0]:
			views[i] += 1
			if not args.groupwise:
				iFile = filesIndicesExploration[2*iBlock+odd][start+i]
				print("  block: {:d}, end time: {:.6f}, image: {:2d} ({:2d}, {:2d}), iFile: {:3d}, duration: {:}".format(
					iMove, t, i, i%5-2, i//5-2, iFile, hits[i]))
		hits[np.where(end)[0]] = 0 # reset counter for images not currently being viewed
		hits += newHits
		#print("{:d}, {:d}".format(x, y))
	except:
		raise
		print("bad line:", row) # ignore bad lines in yokeFile (including header)
if args.groupwise:
	print(views.reshape((5,5)))
