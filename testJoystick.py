#!/usr/bin/env python3
from psychopy import visual, event, core
from psychopy.hardware import joystick

win = visual.Window(winType="pyglet", monitor="testMonitor", waitBlanking=True)
joy = joystick.Joystick(0)
joystickThresshold = 0.5

def waitKeys(stimulus, minWait=0):
	"""
	wait for button A
	"""
	stimulus.draw()
	win.flip()
	while not joy.getButton(0):
		stimulus.draw()
		win.flip()
	return ["return"]

def getDirection():
	"""
	direction of response, from joystick, non blocking, like getKeys
	0: no response yet
	-1: end of trial
	"""
	if(event.getKeys(keyList=["escape"])):
		print("gracious abort")
		#sys.exit()
		raise NameError('UserAbort')
	if joy.getButton(0):
		return ["return"]
	x = joy.getX()
	y = -joy.getY()
	l = (x**2+y**2)**0.5
	print("l: {:5.3f}".format(l))

	if l>joystickThresshold:
		if x<0 and abs(x)>abs(y):
			return ["left"]
		elif y<0 and abs(y)>abs(x):
			return ["down"]
		elif y>0 and abs(y)>abs(x):
			return ["up"]
		elif x>0 and abs(x)>abs(y):
			return ["right"]
	return([])

def likertResponse():
	"""
	position on likert scale (1-4) participant is pointing to or 0 for no choice yet
	"""
	keys = getDirection()
	if "left" in keys:
		return 1 # Definitely OLD
	elif "down" in keys:
		return 2 # Probably OLD
	elif "up" in keys:
		return 3 # Probably NEW
	elif "right" in keys:
		return 4 # Definitely NEW
	return 0

	
while event.getKeys() == []:
	xy_joy = (joy.getX(), joy.getY())
	win.flip()

	print("{:6.3f}, {:6.3f}".format(xy_joy[0], xy_joy[1]))
	core.wait(.05)

print("test in 3 s")
core.wait(3)
print("testing")
# wait for joystick return to center and empty key buffer
while likertResponse():
	print("waiting")
	win.flip()
print("end waiting")

